# Class: shibsp
# ===========================
#
# configure an Emerging Technology Apache + Shibboleth SP image
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2018 The Board of Trustees of the Leland Stanford Junior
# University
#
class shibsp (
  $apache_service_enable,
  $apache_service_ensure,
  $shib_dir,
  $shibd_user,
  $shibd_group,
  $httpd_user,
  $httpd_group,
) {

  # add the admin user to the www-data group
  user { 'admin':
    groups  => $httpd_group,
    require => Package['httpd'],
  }

  class { 'apache':
    service_enable      => $apache_service_enable,
    service_ensure      => $apache_service_ensure,
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      vhost_common => '%v %h %l %u %t \"%r\" %>s %b',
      combined_elb => '%v:%p %{X-Forwarded-For}i %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"'
    },
    logroot             => '/dev',
    mpm_module          => 'prefork',
  }

  apache::vhost { 'shib_sp':
    port              => 8080,
    docroot           => '/var/www',
    docroot_owner     => 0,
    docroot_group     => $httpd_group,
    docroot_mode      => '0755',
    servername        => '${ENV_DOMAIN}', # lint:ignore:single_quote_string_with_variables
    serveradmin       => '${ADMIN_MAIL}', # lint:ignore:single_quote_string_with_variables
    access_log_format => 'combined_elb',
    access_log_file   => 'stdout',
    error_log_file    => 'stdout',
    directories       => [
      {
        path           => '/var/www',
        allow_override => ['All'],
      },
    ],
    aliases           => [
      {
        alias => '/shibboleth-sp',
        path  => '/usr/share/shibboleth',
      }
    ],
  }

  class { 'apache::mod::alias': }
  class { 'apache::mod::env': }
  class { 'apache::mod::rewrite': }
  class { 'apache::mod::authn_core': }
  class { 'apache::mod::expires': }
  class { 'apache::mod::headers': }
  class { 'apache::mod::shib': }

  file {
    [
      '/var/log/apache2',
      '/var/lock/apache2',
      '/var/run/apache2',
    ]:

    ensure  => directory,
    owner   => $httpd_user,
    group   => $httpd_group,
    mode    => '0755',
    require => Package['httpd'],
  }

  file { '/etc/apache2/conf.d/platform_env.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/platform_env.conf",
  }

  file {
    [
      '/var/log/shibboleth',
      '/var/cache/shibboleth',
      '/var/run/shibboleth',
    ]:

    ensure  => directory,
    owner   => $shibd_user,
    group   => $shibd_group,
    mode    => '0755',
    require => Class['apache::mod::shib'],
  }

  ## empty files for bind mounts
  file {
    [
      "${shib_dir}/sp-cert.pem",
      "${shib_dir}/sp-key.pem",
      "${shib_dir}/shibboleth2.xml",
    ]:

    ensure  => file,
    owner   => 0,
    group   => $shibd_group,
    mode    => '0640',
    require => Class['apache::mod::shib'],
  }

  $shib_conf_files = [
    'attribute-map.xml',
    'protocols.xml',
    'idp-signing.pem',
    'itlab-signing.pem',
  ]

  $shib_conf_files.each | String $conf_file | {
    if !defined(File[$conf_file]) {
      file { $conf_file:
        ensure  => file,
        path    => "${shib_dir}/${conf_file}",
        owner   => '0',
        group   => '0',
        mode    => '0644',
        source  => "puppet:///modules/${module_name}/${conf_file}",
        require => [
          Class['apache::mod::shib'],
          Package['httpd'],
        ],
      }
    }
  }

  file { '/start.sh':
    ensure => file,
    owner  => 0,
    group  => 0,
    mode   => '0755',
    source => "puppet:///modules/${module_name}/start.sh",
  }

}

